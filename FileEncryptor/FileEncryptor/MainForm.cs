﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using System.Security.Cryptography;

namespace FileEncryptor
{
    public partial class MainForm : Form
    {
        private readonly string _openFileTextBoxDefault;
        private static byte[] _key = new byte[0];
        private const int BlockSize = 512;

        public MainForm()
        {
            InitializeComponent();
            _openFileTextBoxDefault = sourceFileTextBox.Text;

            genKeyButton.Enabled = false;

            // //TODO--удалить строку для отладки
            // sourceFileTextBox.Text = @"C:\Users\shnei\source\repos\Encryptor\binary.bin";
        }

        private void OpenFileBrowseButton_Click(object sender, EventArgs e)
        {
            var res = openFileDialog.ShowDialog();
            switch (res)
            {
                case (DialogResult.OK):
                    sourceFileTextBox.Text = openFileDialog.FileName;
                    break;
                case (DialogResult.Abort):
                case (DialogResult.Cancel):
                case (DialogResult.None):
                    break;
                default:
                    MessageBox.Show("Invalid Dialog Result", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
            }
        }

        private void SaveFileBrowseButton_Click(object sender, EventArgs e)
        {
            var res = saveFileDialog.ShowDialog();
            switch (res)
            {
                case (DialogResult.OK):
                case (DialogResult.Abort):
                case (DialogResult.Cancel):
                case (DialogResult.None):
                    if (sourceFileTextBox.Text.Trim().Length != 0)
                    {
                        try
                        {
                            var srcPath = sourceFileTextBox.Text;
                            var destPath = saveFileDialog.FileName;

                            EncryptFileWithSave(srcPath, destPath);
                        }
                        catch (Exception exception)
                        {
                            MessageBox.Show(exception.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }

                    break;
                default:
                    MessageBox.Show("Invalid Dialog Result", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    break;
            }
        }

        private void OpenFileTextBox_Enter(object sender, EventArgs e)
        {
            if (sourceFileTextBox.Text.Equals(_openFileTextBoxDefault))
                sourceFileTextBox.Text = "";
        }

        private void OpenFileTextBox_Leave(object sender, EventArgs e)
        {
            if (sourceFileTextBox.Text.Trim().Length == 0)
                sourceFileTextBox.Text = _openFileTextBoxDefault;
        }

        private void HelpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //TODO--вывод инструкции
        }

        private void AboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //TODO--вывод информации о приложении
        }

        #region Logic

        private string GetKeyString()
        {
            //LINQ
            string[] strByteArr = _key.Select(b => b.ToString("X")).ToArray();

            string res = "";
            foreach (var s in strByteArr)
                res += s;

            return res;
        }

        public void EncryptFileWithSave(string srcPath, string destPath)
        {
            if (!File.Exists(srcPath)) throw new FileNotFoundException();


            //Создаем объект для шифрования, генерируем фиксированный ключ
            Rijndael encryption = Rijndael.Create();

            var random = new Random().Next() ^ long.MinValue;

            Rfc2898DeriveBytes key = new Rfc2898DeriveBytes(random.ToString(), 128);

            encryption.Key = key.GetBytes(32);
            encryption.IV = key.GetBytes(16);

            _key = encryption.Key;

            genKeyButton.Enabled = true;

            //создаем объекты для чтения и записи

            BinaryReader binarySourceReader = new BinaryReader(new FileStream(srcPath, FileMode.Open, FileAccess.Read));
            BinaryWriter binaryDestWriter =
                new BinaryWriter(new FileStream(destPath, FileMode.OpenOrCreate, FileAccess.Write));

            MemoryStream ms = new MemoryStream(BlockSize);
            CryptoStream cs = new CryptoStream(ms, encryption.CreateEncryptor(), CryptoStreamMode.Write);

            // long srcFileSize = new FileInfo(srcPath).Length;

            // long byteRead = 0;
            int thisIterationByteRead = 0;

            while (thisIterationByteRead != 0)
            {
                byte[] block = binarySourceReader.ReadBytes(BlockSize);

                thisIterationByteRead = (from b in block
                    where b != 0
                    select b).Count();

                // byteRead += thisIterationByteRead;

                cs.Write(block, 0, (int) thisIterationByteRead);
                ms.Read(block, 0, thisIterationByteRead);

                binaryDestWriter.Write(block, 0, thisIterationByteRead);
            }


            cs.Dispose();
            ms.Dispose();
            binarySourceReader.Dispose();
            binaryDestWriter.Dispose();

            MessageBox.Show("Encrypted file is saved", "OK", MessageBoxButtons.OK, MessageBoxIcon.None);
        }

        #endregion

        private void GenKeyButton_Click(object sender, EventArgs e)
        {
            var destPath = saveFileDialog.FileName;
            destPath = destPath.Substring(0, destPath.LastIndexOf("\\")) + "\\key.txt";
            var f = new StreamWriter(new FileStream(destPath, FileMode.Create, FileAccess.Write));
            f.WriteLine(GetKeyString());


            MessageBox.Show("Key is generated and saved to:\n" + destPath, "OK", MessageBoxButtons.OK,
                MessageBoxIcon.None);

            f.Dispose();
        }

        #region Drag&Drop

        private void DragAndDropPanel_DragDrop(object sender, DragEventArgs e)
        {
            sourceFileTextBox.Text = (e.Data.GetData(DataFormats.FileDrop) as string[])[0];
            dragAndDropPanel.Visible = false;
        }

        private void DragAndDropPanel_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
        }

        private void DragAndDropPanel_DragLeave(object sender, EventArgs e)
        {
            dragAndDropPanel.Visible = false;
        }

        private void MainForm_DragEnter(object sender, DragEventArgs e)
        {
            dragAndDropPanel.Visible = true;
        }

        #endregion
    }
}